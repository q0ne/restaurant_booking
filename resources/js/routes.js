import ExampleComponent from './components/ExampleComponent.vue';
import ProductComponent from "./components/ProductComponent.vue";
import ProductCreateComponent from "./components/ProductCreateComponent.vue";
import AnotherComponent
    from "./components/AnotherComponent.vue";

const routes = [
    { path: '/', component: ExampleComponent },
    { path: '/products', component: ProductComponent },
    { path: '/product/create', component: ProductCreateComponent },
];

export default routes;
