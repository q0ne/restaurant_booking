<?php

namespace App\Observers;

use App\Models\Restaurant;
use Illuminate\Support\Facades\Log;

class RestaurantObserver
{
    public function creating(Restaurant $restaurant)
    {
        Log::info('RestaurantObserver: creating event triggered.');
        if (is_null($restaurant->lat) && is_null($restaurant->long)) {
            $fullAddress = $restaurant->address_street . ', '
                . $restaurant->address_postcode . ', '
                . $restaurant->city->name;
            $result = app('geocoder')->geocode($fullAddress)->get();
            if ($result->isNotEmpty()) {
                $coordinates = $result[0]->getCoordinates();
                $restaurant->lat = $coordinates->getLatitude();
                $restaurant->long = $coordinates->getLongitude();
            }
        }}

}
