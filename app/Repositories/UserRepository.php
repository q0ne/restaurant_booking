<?php

namespace App\Repositories;

use App\Repositories\Interfaces\UserRepositoryInterface;
use Illuminate\Database\Eloquent\Model;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function findUserByEmail(string $email): ?Model
    {
        return $this->model->query()->where('email', $email)->first();
    }

    public function findUserByPhone(string $phone): ?Model
    {
        return $this->model->query()->where('phone', $phone)->first();
    }

    public function findUserByEmailOrPhone(string $email, string $phone): ?Model
    {
        return $this->model->query()->where('email', $email)->orWhere('phone', $phone)->first();
    }


}