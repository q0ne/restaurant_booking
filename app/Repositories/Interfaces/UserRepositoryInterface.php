<?php

namespace App\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Model;

interface UserRepositoryInterface extends EloquentRepositoryInterface
{
    public function findUserByEmail(string $email): ?Model;

    public function findUserByPhone(string $phone): ?Model;

    public function findUserByEmailOrPhone(string $email, string $phone): ?Model;
}