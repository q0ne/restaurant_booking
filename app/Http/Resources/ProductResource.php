<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property mixed $id
 * @property mixed $name
 * @property mixed $price
 * @property mixed $description
 * @property mixed $image

 *
 * @OA\Schema(
 *     schema="ProductResource",
 *     title="ProductResource",
 *     description="Product resource representation",
 *     @OA\Property(property="id", type="integer", example="1"),
 *     @OA\Property(property="name", type="string", example="Product A"),
 *     @OA\Property(property="description", type="string", example="Product A description"),
 *     @OA\Property(property="price", type="number", format="integer", example="19.99"),
 *     @OA\Property(property="image", type="string", example="product-a.jpg"),
 * )
 */
class ProductResource extends JsonResource
{

    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'price' => $this->price,
            'description' => $this->description,
            'image' => $this->image,
//            'created_at' => $this->created_at,
//            'updated_at' => $this->updated_at,
        ];
    }
}
