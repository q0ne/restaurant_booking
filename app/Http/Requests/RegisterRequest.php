<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
        'first_name'=> 'required|string|max:255',
        'last_name'=> 'required|string|max:255',
        'middle_name'=> 'nullable|string',
        'email'=> 'nullable|email|unique:users,email',
        'phone'=> 'nullable|phone|unique:users,phone',
        'password'=> 'required|min:8|confirmed',
        'avatar'=> 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
    }
}
