<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
/**
 * @OA\Schema(
 *     schema="ProductRequest",
 *     title="ProductRequest",
 *     description="Product creation/update request",
 *     @OA\Property(property="name", type="string", example="Product A"),
 *     @OA\Property(property="price", type="number", format="float", example="19.99"),
 *     @OA\Property(property="description", type="string", example="Product A description"),
 *     @OA\Property(property="image", type="file", example="product-a.jpg"),
 * )
 */
class ProductRequest extends FormRequest
{

    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'description' => 'required|string',
            'price' => 'required|numeric',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
    }
}
