<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Models\Product;
use App\Services\ProductService;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="OrynTap API",
 *     description="OrynTap API Documentation",
 *     @OA\Contact(
 *     email="dsa@dsa.dsa"
 *    ),
 *     @OA\License(
 *     name="Apache 2.0",
 *     url="http://www.apache.org/licenses/LICENSE-2.0.html"
 *      )
 *  ),
 * @OA\Server(
 *     url=L5_SWAGGER_CONST_HOST,
 *     description="OrynTap API Server"
 * ),
 */
class ProductController extends Controller
{
    private $productService;
    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    /**
     * @OA\Get(
     *     path="/api/products",
     *     summary="Get all products",
     *     tags={"Products"},
     *     @OA\Response(
     *         response=200,
     *         description="List of products",
     *         @OA\JsonContent(
     *             type="array",
     *             @OA\Items(ref="#/components/schemas/ProductResource")
     *         )
     *     ),
     *     security={}
     * )
     */
    public function index()
    {
        $result = $this->productService->getAllProducts();

        return response()->json([
            'success'=> true,
            'data' => $result
        ], ResponseAlias::HTTP_OK);
    }
    /**
     * @OA\Post(
     *     path="/api/products",
     *     summary="Create a new product",
     *     tags={"Products"},
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/ProductRequest")
     *     ),
     *     @OA\Response(
     *         response=201,
     *         description="Product created successfully",
     *         @OA\JsonContent(ref="#/components/schemas/ProductResource")
     *     ),
     * )
     */
    public function store(ProductRequest $request)
    {
        $result = $this->productService->createProduct($request->validated());

        return response()->json([
            'success'=> true,
            'data' => $result,
            'message' => 'Product created successfully'
        ], ResponseAlias::HTTP_CREATED);
    }

    /**
     *
     * @OA\Get(
     *     path="/api/products/{id}",
     *     summary="Get a specific product",
     *     tags={"Products"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the product",
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Product details",
     *         @OA\JsonContent(ref="#/components/schemas/ProductResource")
     *     ),
     *     security={}
     * )
     */
    public function show(Product $product)
    {
        $result  = $this->productService->getProduct($product);
        return response()->json([
            'success'=> true,
            'data' => $result
        ], ResponseAlias::HTTP_OK);
    }

    /**
     * @OA\Put(
     *     path="/api/products/{id}",
     *     summary="Update a specific product",
     *     tags={"Products"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the product",
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\RequestBody(
     *         required=true,
     *         @OA\JsonContent(ref="#/components/schemas/ProductRequest")
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Product updated successfully",
     *         @OA\JsonContent(ref="#/components/schemas/ProductResource")
     *     ),
     * )
     */

    public function update(ProductRequest $request, Product $product)
    {
        $result = $this->productService->updateProduct($product, $request->validated());

        return response()->json([
            'success'=> true,
            'data' => $result
        ], ResponseAlias::HTTP_OK);
    }

    /**
     * @OA\Delete(
     *     path="/api/products/{id}",
     *     summary="Delete a specific product",
     *     tags={"Products"},
     *     @OA\Parameter(
     *         name="id",
     *         in="path",
     *         required=true,
     *         description="ID of the product",
     *         @OA\Schema(type="integer")
     *     ),
     *     @OA\Response(
     *         response=204,
     *         description="Product deleted successfully"
     *     ),
     * )
     */

    public function destroy(Product $product): \Illuminate\Http\JsonResponse
    {
        $result = $this->productService->deleteProduct($product);

        return response()->json([
            'success'=> true,
            'message' => $result
        ], ResponseAlias::HTTP_NO_CONTENT);
    }

    public function export(): \Symfony\Component\HttpFoundation\BinaryFileResponse
    {
       return $this->productService->exportProducts();
    }
}
