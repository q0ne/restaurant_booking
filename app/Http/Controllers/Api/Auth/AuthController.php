<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class AuthController
{
    protected UserService $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function register(RegisterRequest $request): \Illuminate\Http\JsonResponse
    {
        $result = $this->userService->registerUser($request->validated());

        return response()->json([
            'success' => true,
            'message' => __('User created successfully'),
            'data' => [
                'token' => $result['token'],
                'user' => $result['user'],
                    ]]);
    }
    public function login(LoginRequest $request): \Illuminate\Http\JsonResponse
    {
        $result = $this->userService->loginUser($request->validated());
            return response()->json([
                'success' => true,
                'message' => __('Login success'),
                'data' => [
                    'token' => $result['token'],
                    'user' => $result['user']
                ],
            ]);
    }

    public function logout(): \Illuminate\Http\JsonResponse
    {
       $this->userService->logoutUser();

        return response()->json([
            'success' => true,
            'message' => __('You successfully logged out.')
        ], ResponseAlias::HTTP_OK);
    }
}