<?php

namespace App\Services;

use App\Models\User;
use App\Repositories\Interfaces\UserRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class UserService
{
    protected UserRepositoryInterface $repository;

    public function __construct(UserRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function registerUser(array $data): array
    {
        $user = User::create($data);
        $token = $user->createToken($user->email)->plainTextToken;
        return [
            'token' => $token,
            'user' => $user,
        ];
    }

    public function loginUser(array $data): array
    {
        $user = $this->checkUserExists($data);
        $this->checkUserPasswordCorrect($user, $data['password']);
        $token = $user->createToken($user->email)->plainTextToken;
        return [
            'token' => $token,
            'user' => $user,
        ];
    }

    private function checkUserPasswordCorrect($user, $requestPassword): void
    {
        if (!$user || !Hash::check($requestPassword, $user->password)) {
            response()->json([
                'success' => false,
                'message' => __('Неверное имя пользователя или пароль. Пожалуйста, попробуйте снова'),
            ], ResponseAlias::HTTP_NOT_FOUND);
        }
    }

    private function checkUserExists(array $user): ?\Illuminate\Database\Eloquent\Model
    {
        $user = $this->repository->findUserByEmail($user['email']);
        if (!$user) {
            response()->json([
                'success' => false,
                'message' => __('Неверное имя пользователя или пароль. Пожалуйста, попробуйте снова'),
            ], ResponseAlias::HTTP_NOT_FOUND);
        }
        return $user;
    }

    public function logoutUser(): void
    {
        $user = Auth::guard('sanctum')->user();
        $user?->tokens()->delete();
    }
}