<?php

namespace App\Services;

use App\Exports\ProductsExport;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use App\Repositories\Interfaces\ProductRepositoryInterfaceInterface;
use Maatwebsite\Excel\Facades\Excel;


class ProductService
{
    protected ProductRepositoryInterfaceInterface $productRepository;

    public function __construct(ProductRepositoryInterfaceInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function getAllProducts(): \Illuminate\Http\Resources\Json\AnonymousResourceCollection
    {
        return ProductResource::collection($this->productRepository->getAll());
    }

    public function createProduct(array $data)
    {
        if (isset($data['image'])) {
            $data['image'] = $this->generateImage($data['image']);
        }
        return new ProductResource(Product::create($data));
    }

    public function getProduct(Product $product): ProductResource
    {
        return new ProductResource($product);
    }

    public function updateProduct(Product $product, array $data): ProductResource
    {
        if (isset($data['image'])) {
            $data['image'] = $this->generateImage($data['image']);
        }
        $product->update($data);
        return new ProductResource($product);
    }

    public function deleteProduct(Product $product): string
    {
        $product->delete();
        return 'Product deleted successfully';
    }

    public function exportProducts(): \Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        return Excel::download(new ProductsExport, 'products.xlsx');
    }

    private function generateImage($image): ?string
    {
        if (!$image) {
            return null;
        }
        $image_name = time() . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/images/products');
        $image->move($destinationPath, $image_name);
        return $image_name;
    }


}