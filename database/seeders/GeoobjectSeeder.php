<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class GeoobjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $geoobjects = [
            [
                'city_id' => 1,
                'name' => 'Кок-Тобе',
                'lat' => 43.238949,
                'long' => 76.889709,
            ],
            [
                'city_id' => 1,
                'name' => 'Апорт',
                'lat' => 43.238949,
                'long' => 76.889709,
            ],
            [
                'city_id' => 2,
                'name' => 'Парк 28 Панфиловцев',
                'lat' => 51.169392,
                'long' => 71.449074,
            ],
            [
                'city_id' => 2,
                'name' => 'Парк Горького',
                'lat' => 51.169392,
                'long' => 71.449074,
            ],
        ];

        foreach ($geoobjects as $geoobject) {
            \App\Models\Geoobject::create($geoobject);
        }
    }
}
