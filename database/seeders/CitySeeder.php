<?php

namespace Database\Seeders;

use App\Models\City;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $cities = [
            [
                'name' => json_encode([
                    'ru' => 'Алматы',
                    'en' => 'Almaty',
                ]),
                'lat' => 43.238949,
                'long' => 76.889709,
            ],
            [
                'name' => json_encode([
                    'ru' => 'Астана',
                    'en' => 'Astana',
                ]),
                'lat' => 51.169392,
                'long' => 71.449074,
            ],
        ];

        foreach ($cities as $city) {
            City::create($city);
        }
    }
}
