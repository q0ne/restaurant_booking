<?php

use App\Http\Controllers\Api\ProductController;
use Illuminate\Support\Facades\Route;

Route::get('products/export', [ProductController::class, 'export']);
Route::apiResource('products', ProductController::class);

Route::post('register', [\App\Http\Controllers\Api\Auth\AuthController::class, 'register']);
Route::post('/products', [ProductController::class, 'store']);
Route::get('/products', [ProductController::class, 'index']);
Route::post('login', [\App\Http\Controllers\Api\Auth\AuthController::class, 'login']);

Route::middleware('auth:sanctum')->group(function () {
    Route::post('owner/restaurants', [\App\Http\Controllers\Api\Owner\RestaurantController::class, 'store']);
});

    Route::get('log-viewer', [\Rap2hpoutre\LaravelLogViewer\LogViewerController::class, 'index']);
